#!/bin/bash

TECA_SRC_DIR=$PWD/TECA
BLD_DIR="$TECA_SRC_DIR/bld"

DO_CLEAN=$1

# clone TECA if it hasn't been done already
if [ ! -d "$TECA_SRC_DIR" ]; then
  echo "Cloning TECA into $TECA_SRC_DIR"
  git clone https://github.com/LBL-EESA/TECA.git $TECA_SRC_DIR

  # flag that we need to do a clean build
  DO_CLEAN="clean" 
fi

# if the "clean" flag has been given, do a clean build and re-run cmake
if [ "$DO_CLEAN" == "clean" ]; then
  # remove the build directory
  if [ -d "$BLD_DIR" ]; then
    rm -rf "$BLD_DIR"
  fi
  # run cmake
  docker run --rm -v "$TECA_SRC_DIR":/opt/TECA -t teca-jupyter-dev /bin/bash -c "mkdir -p /opt/TECA/bld; cd /opt/TECA/bld; cmake -DNETCDF_DIR=/opt/conda -DBOOST_ROOT=/opt/conda -DCMAKE_INSTALL_PREFIX=/opt/TECA/install/ .."
fi
# run the build
docker run --rm -v "$TECA_SRC_DIR":/opt/TECA -t teca-jupyter-dev /bin/bash -c "cd /opt/TECA/bld; make -j 8 install"
