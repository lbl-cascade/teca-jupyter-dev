# Copyright (c) Jupyter Development Team.
# Distributed under the terms of the Modified BSD License.
ARG BASE_CONTAINER=jupyter/minimal-notebook
FROM $BASE_CONTAINER

LABEL maintainer="Travis A. O'Brien <TAOBrien@lbl.gov>"

USER root

# ffmpeg for matplotlib anim
RUN apt-get update && \
    apt-get install -y --no-install-recommends vim ffmpeg wget mercurial git && \
    rm -rf /var/lib/apt/lists/*

# Install FFTW3
RUN cd /tmp && \
    wget http://www.fftw.org/fftw-3.3.8.tar.gz && \
    tar -xzf fftw-3.3.8.tar.gz && rm fftw-3.3.8.tar.gz && \
    cd fftw-3.3.8 && \
    CFLAGS=-fPIC CXXFLAGS=-fPIC ./configure && \
    make install -j 8 && \
    cd && \
    rm -rf /tmp/fftw-3.3.8

USER $NB_UID

# copy the condarc file that will make conda-forge the default
COPY .condarc /home/$NB_USER/.condarc

# Install Python 3 packages
# Remove pyqt and qt pulled in for matplotlib since we're only ever going to
# use notebook-friendly backends in these images
RUN conda install -c conda-forge --quiet --yes \
    'conda-forge::blas=*=openblas' \
    'conda-forge::ipywidgets=7.4*' \
    'conda-forge::pandas=0.23*' \
    'conda-forge::numexpr=2.6*' \
    'conda-forge::matplotlib=2.2*' \
    'conda-forge::scipy=1.2*' \
    'conda-forge::seaborn=0.9*' \
    'conda-forge::scikit-learn=0.20*' \
    'conda-forge::scikit-image=0.14*' \
    'conda-forge::sympy=1.1*' \
    'conda-forge::cython=0.28*' \
    'conda-forge::patsy=0.5*' \
    'conda-forge::statsmodels=0.9*' \
    'conda-forge::cloudpickle=0.5*' \
    'conda-forge::dill=0.2*' \
    'conda-forge::numba=0.38*' \
    'conda-forge::bokeh=0.13*' \
    'conda-forge::sqlalchemy=1.2*' \
    'conda-forge::hdf5=1.10*' \
    'conda-forge::h5py' \
    'conda-forge::libpng' \
    'conda-forge::ipympl' \
    'conda-forge::vincent=0.4.*' \
    'conda-forge::beautifulsoup4=4.6.*' \
    'conda-forge::protobuf=3.*' \
    'conda-forge::xarray' \
    'conda-forge::emcee' \
    'conda-forge::cartopy=0.17*' \
    'conda-forge::geopandas' \
    'conda-forge::pynio' \
    'conda-forge::iris' \
    'conda-forge::gdal' \
    'conda-forge::krb5' \
    'conda-forge::expat' \
    'conda-forge::gitpython' \
    'conda-forge::netCDF4' \
    'conda-forge::mpi4py=2*' \
    'conda-forge::tornado=5.1.*' \
    'conda-forge::basemap' \
    'cascade::climextremes' \
    'conda-forge::libgfortran' \
    'conda-forge::xlrd'  && \
    conda remove --quiet --yes --force qt pyqt && \
    conda clean -tipsy && \
    # Activate ipywidgets extension in the environment that runs the notebook server
    jupyter nbextension enable --py widgetsnbextension --sys-prefix && \
    # Also activate ipywidgets extension for JupyterLab
    # Check this URL for most recent compatibilities
    # https://github.com/jupyter-widgets/ipywidgets/tree/master/packages/jupyterlab-manager
    jupyter labextension install @jupyter-widgets/jupyterlab-manager@^0.38.1 && \
    jupyter labextension install jupyterlab_bokeh@0.6.3 && \
    jupyter labextension install jupyterlab_vim && \
    jupyter labextension install jupyter-matplotlib && \
    npm cache clean --force && \
    rm -rf $CONDA_DIR/share/jupyter/lab/staging && \
    rm -rf /home/$NB_USER/.cache/yarn && \
    rm -rf /home/$NB_USER/.node-gyp && \
    fix-permissions $CONDA_DIR && \
    fix-permissions /home/$NB_USER

RUN pip install git+https://bitbucket.org/lbl-cascade/fastkde && \
    pip install f90nml 

# Install facets which does not have a pip or conda package at the moment
RUN cd /tmp && \
    git clone https://github.com/PAIR-code/facets.git && \
    cd facets && \
    jupyter nbextension install facets-dist/ --sys-prefix && \
    cd && \
    rm -rf /tmp/facets && \
    fix-permissions $CONDA_DIR && \
    fix-permissions /home/$NB_USER

USER root

# Install SHTns
RUN cd /tmp && \
    hg clone https://bitbucket.org/nschaeff/shtns -r 80894664d451e96c82c431b94fbc9a2902714a3f && \
    cd shtns && \
    CFLAGS=-fPIC CXXFLAGS=-fPIC ./configure --disable-openmp --enable-python && \
    make install -j 8 && \
    cd && \
    rm -rf /tmp/shtns
 
USER $NB_UID

# Import matplotlib the first time to build the font cache.
ENV XDG_CACHE_HOME /home/$NB_USER/.cache/
RUN MPLBACKEND=Agg python -c "import matplotlib.pyplot" && \
    fix-permissions /home/$NB_USER



# set the R installation directory
ENV R_HOME=$CONDA_DIR/lib/R
# import climextremes the first time around to finalize the install
RUN python -c "import climextremes"

ENV PROJ_LIB=$CONDA_DIR/share/proj/

USER $NB_UID
