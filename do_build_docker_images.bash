#!/bin/bash

echo "Building climate-notebook image"
docker build -f climate-notebook.Dockerfile --tag climate-notebook $PWD

echo "Building teca-jupyter-dev from climate-notebook image"
docker build -f teca-jupyter-dev.Dockerfile --tag teca-jupyter-dev $PWD
