#!/usr/bin/env python
import teca
import numpy as np
import xarray as xr
import pylab as PP
import cartopy.crs as ccrs

class TECAPyPlot:
    
    def __init__(self,
                 input_var,
                 title = None,
                 data_transform = ccrs.PlateCarree(),
                 plot_projection = ccrs.PlateCarree(),):
        self.input_var = input_var
        self.data_transform = data_transform
        self.plot_projection = data_transform
        self.title = title
    
    def get_report_callback(self):
        input_var = self.input_var
        def report(port, md_in):
            md = md_in[0]
            mdp = teca.teca_metadata(md)
            
            # get a list of all available attributes
            # TODO: this is a workaround for running list(mdp['attributes'][input_var])
            metadata_string = str(mdp['attributes'][input_var]).split('\n')
            att_names = [ m.split('=')[0].rstrip() for m in metadata_string]
            att_names = [ m for m in att_names if m != '' ]
            
            self.attrs = {}
            # extract attributes and put them in a dict
            for att in att_names:
                self.attrs[att] = mdp['attributes'][input_var][att]#[0]
                
            return md
        return report
    
    def get_request_callback(self):
        input_var = self.input_var
        def request(port, md_in, req_in):
            req = teca.teca_metadata(req_in)
            req['arrays'] = [input_var]
            return [req]
        return request

    def get_execute_callback(self):
        input_var = self.input_var
        plot_projection = self.plot_projection
        data_transform = self.data_transform
        def execute(port, data_in, req):
            # get the input mesh
            in_mesh = teca.as_teca_cartesian_mesh(data_in[0])

            # get coordinate variables
            lon = np.array(in_mesh.get_x_coordinates())
            lat = np.array(in_mesh.get_y_coordinates())

            # get the data
            arrays = in_mesh.get_point_arrays()
            var = np.reshape(arrays[input_var], [len(lat), len(lon)])

            # put the data into an xarray object
            var_ds = xr.DataArray(var,
                                  coords = dict(lat = lat, lon = lon),
                                  dims = ('lat', 'lon'))
            # put the previously-derived attributes into the xarray dict
            var_ds.attrs = self.attrs

            # use xarray to generate a plot
            fig, ax = PP.subplots(subplot_kw = dict(projection = plot_projection), figsize = (8,4))
            var_ds.plot(ax = ax, transform = data_transform)
            ax.coastlines(color = 'gray', alpha = 2/3)
            
            # add a title
            if self.title is not None:
                ax.set_title(self.title)
            
            ax.set_aspect('auto')
            PP.show()

            return in_mesh
        return execute