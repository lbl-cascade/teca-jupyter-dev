FROM climate-notebook

USER root

# install cmake
RUN apt-get update && apt-get install -y --no-install-recommends \
                                        cmake \
                                        findutils \
                                        build-essential \
                                        gfortran \
                                        libatlas-base-dev \
                                        libc6-dev \
                                        libexpat1-dev \
                                        libopenblas-dev \
                                        libpcre3-dev \
                                        pkg-config \
                                        libnetcdf-dev \
                                        libnetcdff-dev \
                                        libudunits2-dev \
                                        libboost-all-dev \
                                        gdb \
                                        valgrind \
                                        subversion \
                                        vim \
                                        zlib1g-dev && \
    rm -rf /var/lib/apt/lists/*

USER $NB_UID

# install dependencies for TECA
RUN conda install -c conda-forge --quiet --yes --no-update-deps\
    'conda-forge::blas=*=openblas' \
    'conda-forge::numpy' \
    'conda-forge::mpi4py' \
    'conda-forge::netCDF4' \
    'conda-forge::boost' \
    'conda-forge::matplotlib' \
    'conda-forge::python-dateutil' \
    'conda-forge::cython' \
    'conda-forge::swig' \
    'conda-forge::pyparsing' \
    'conda-forge::cycler' \
    'conda-forge::pytz' && \
    conda clean -tipsy && \
    fix-permissions $CONDA_DIR

#    'conda-forge::openmpi' \

USER $NB_UID
