#!/bin/bash

fix_url(){
  gawk '/^\s+http:\/\/.*:8888\/.*/ { new_url = gensub(/http:\/\/.*:8888\//, "http://localhost:10001/", "g"); print new_url; next} /.*/ {print $0;}'
}

TECA_SRC_DIR=$PWD/TECA
TECA_DATA_DIR=$PWD/TECA_data

WORKDIR=$PWD/work
mkdir -p $WORKDIR
docker run --rm -p 10001:8888 \
  -e PYTHONPATH=/opt/TECA/install/lib \
  -e LD_LIBRARY_PATH=/opt/TECA/install/lib \
  -e JUPYTER_ENABLE_LAB=yes \
  -v $WORKDIR:/home/jovyan/ \
  -v "$TECA_SRC_DIR":/opt/TECA\
  -v "$TECA_DATA_DIR":/opt/TECA_data\
  teca-jupyter-dev 2>&1 | fix_url
  
