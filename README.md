This repository contains scripts, files, and a recipe for using docker and a set of scripts to get TECA (and in particular its python bindings) to work in a docker environment that has Jupyter and many climate-related utilities installed.

# Getting Started
 1. Clone this repository: `git clone https://bitbucket.org/lbl-cascade/teca-jupyter-dev.git`
 2. Build the docker images: `bash do_build_docker_images.bash`
 3. Clone and build TECA in the `teca-jupyter-dev` environment: `bash do_build_teca.bash`
 4. Run jupyterlab: `bash start_jupyter_lab.bash` 
 5. Open jupyterlab in your browser ( `http://localhost:10001/?token=....`), where the token string is taken from the command line output.
 5. Run TECA in a notebook (see `work/Absolute AR Detector.ipynb`)!

# Key Ingredients
 * A Docker file for the `climate-notebook` docker environment: `climate-notebook.Dockerfile`
 * A Docker file that builds on the `climate-notebook` environment to install TECA-related dependencies: `teca-jupyter-dev.Dockerfile`
 * A script for building the docker images: `do_build_docker_images.bash`
 * A script for building TECA in the docker environment: `do_build_teca.bash`
 * A script for running jupyterlab in the docker environment: `start_jupyter_lab.bash`

# Requirements
 * [Docker](https://docs.docker.com/get-started/)
 * The `git` command line utility
 * `bash`
 * 5-10GB of free space for the docker image
